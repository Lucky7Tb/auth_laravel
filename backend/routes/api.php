<?php

use Illuminate\Http\Request;

Route::post('login', 'API\AuthController@login');
Route::post('register', 'API\AuthController@register');
Route::post('verifycaptcha', 'API\AuthController@verifycaptcha');

Route::group(['middleware' => 'auth:api'], function(){
        Route::get('logout', 'API\AuthController@logout');
        Route::get('details', 'API\AuthController@details');
});