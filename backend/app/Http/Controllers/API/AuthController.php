<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Auth;
use Validator;

class AuthController extends Controller
{
    public function login(Request $request){
        try {
             $validator = $request->validate([
                'email' => 'required|email',
                'password' => 'required'
            ]);
            if(Auth::attempt(['email' => request('email'), 'password' => request('password')])){ 
                $user = Auth::user(); 
                $success['token'] =  $user->createToken('App')->accessToken; 
                return response()->json([
                    'success' => $success, 'user' => $user
                ], 200); 
            } 
            else{ 
                return response()->json([
                    'error' => 'email atau password salah'
                ], 401); 
            } 
        } catch (Exception $error) {
            return response()->json([
                'message' => 'Terjadi error pada server.',
                'error' => $error->getMessage()
            ], 500);
        }
    }

    public function register(Request $request){
        try {
            $validator = $request->validate([
                'name' => 'required',
                'email' => 'required|email|unique:users,email',
                'password' => 'required|min:6'
            ]);
    
            $dataUser = new User;
            $dataUser->name = $request->name;
            $dataUser->email = $request->email;
            $dataUser->password = bcrypt($request->password);
            $dataUser->save();
            
            return response()->json([
                'message' => 'Berhasil register'
            ], 200);
        } catch (Exception $error) {
              return response()->json([
                'message' => 'Terjadi error pada server.',
                'error' => $error->getMessage()
            ], 500);
        }
    }

    public function logout(Request $request){
        try {
            $token = $request->user()->token();
            $token->revoke();
            return response()->json([
                'message' => 'Berhasil Logout',
            ], 200);
        } catch (Exception $error) {
            return response()->json([
                'message' => 'Terjadi error pada server.',
                'error' => $error->getMessage()
            ], 500);
        }
    }

    public function details(Request $request){
        try {
            return response()->json([
                'user' => Auth::user()
            ], 200);
        } catch (Exception $error) {
             return response()->json([
                'message' => 'Terjadi error pada server.',
                'error' => $error->getMessage()
            ], 500);
        }
    }

    public function verifycaptcha(Request $request){
        try {
            $guzzleHttp = new \GuzzleHttp\Client();
            $response = $guzzleHttp->request('POST', 'https://www.google.com/recaptcha/api/siteverify', [
                'form_params' => [
                    'secret' => $request->secret,
                    'response' => $request->response
                ]
            ]);

            $result = json_decode($response->getBody()->getContents());
            return response()->json([
                'message' => 'Berhasil.',
                'response' => $result
            ], 200);
        } catch (Exception $error) {
              return response()->json([
                'message' => 'Terjadi error pada server.',
                'error' => $error->getMessage()
            ], 500);
        }
    }
}
