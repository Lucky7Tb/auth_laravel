const loginButton = document.querySelector('.loginButton');
const emailFieldLogin = document.getElementById('emailLogin');
const passwordFieldLogin = document.getElementById('passwordLogin');
const preloader = document.querySelector('.preloader');

window.addEventListener('load', function () {
    let data = JSON.parse(localStorage.getItem("user"));
    preloader.style.display = 'none';
    console.log(data);
    if (data !== null) {
        window.location.href = "index.html";
    }
});

loginButton.addEventListener('click', onLogin);

async function onLogin() {
    try {
        preloader.style.opacity = 0.5;
        preloader.style.display = null;

        let response = await fetch('http://localhost:8000/api/login', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
            },
            body: JSON.stringify({
                'email': emailFieldLogin.value,
                'password': passwordFieldLogin.value
            })
        })

        let result = await response.json();

        console.log(result);

        preloader.style.opacity = 1;
        preloader.style.display = 'none';

        if (result.messsage === 'email atau password salah') {
            M.toast({ html: 'Email atau password salah', classes: 'rounded red darken-3' });
        } else if (result.messsage === 'The given data was invalid.') {
            if (result.errors.email) {
                M.toast({ html: result.errors.email[0], classes: 'rounded red darken-3' });
            }

            if (result.errors.password) {
                M.toast({ html: result.errors.password[0], classes: 'rounded red darken-3' });
            }
        } else {
            let responseData = {
                token: response.success.token,
                user: response.user
            }
            window.localStorage.setItem('user', JSON.stringify(responseData));
            window.location.href = 'index.html';
        }
    } catch (error) {
        console.log(error)
        M.toast({ html: 'Terjadi kesalahan pada server', classes: 'rounded red darken-3' });
    }

    // .then(response => {
    //     if (response.status === 401) {
    //         M.toast({ html: 'Email atau password salah', classes: 'rounded red darken-3' })
    //     }
    //     return response.json()
    // })
    // .then(response => {
    //     preloader.style.opacity = 1;
    //     preloader.style.display = 'none';
    //     if (response.message === 'The given data was invalid.') {
    //         if (response.errors.email) {
    //             M.toast({ html: response.errors.email[0], classes: 'rounded red darken-3' })
    //         }

    //         if (response.errors.password) {
    //             M.toast({ html: response.errors.password[0], classes: 'rounded red darken-3' })
    //         }
    //     } else {
    //         let responseData = {
    //             token: response.success.token,
    //             user: response.user
    //         }
    //         window.localStorage.setItem('user', JSON.stringify(responseData));
    //         window.location.href = 'index.html';
    //     }
    // })
    // .catch(error => M.toast({ html: 'Terjadi kesalahan pada server', classes: 'rounded red darken-3' }))

}