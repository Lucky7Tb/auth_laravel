const registerButton = document.querySelector('.registerButton');

const nameField = document.getElementById('nameRegister');
const emailField = document.getElementById('emailRegister');
const passwordField = document.getElementById('passwordRegister');
const preloader = document.querySelector('.preloader');
let googleRecaptcha;

window.addEventListener('load', function () {

    googleRecaptcha = grecaptcha.render('captcha', {
        'sitekey': '6LeyStIUAAAAAAvC6P-gZry8jrdNRIbsADQRAEf7',
        'theme': 'light'
    });

    let data = JSON.parse(localStorage.getItem("user"));

    console.log(data);
    if (data !== null) {
        window.location.href = "index.html";
    }

    preloader.style.display = 'none';

});

registerButton.addEventListener('click', onRegister);

function onRegister(event) {
    event.preventDefault();
    preloader.style.opacity = 0.5;
    preloader.style.display = null;
    let token = grecaptcha.getResponse(googleRecaptcha);
    verifyUserCaptcha(token);
}

async function register() {
    try {
        let response = await fetch('http://localhost:8000/api/register', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
            },
            body: JSON.stringify({
                'name': nameField.value,
                'email': emailField.value,
                'password': passwordField.value
            })
        });
        let result = await response.json();
        console.log(result);
        preloader.style.opacity = 1;
        preloader.style.display = 'none';
        if (result.message === 'The given data was invalid.') {
            if (result.errors.name) {
                M.toast({ html: result.errors.name[0], classes: 'rounded red darken-3' });
            }

            if (result.errors.email) {
                M.toast({ html: result.errors.email[0], classes: 'rounded red darken-3' });
            }

            if (result.errors.password) {
                M.toast({ html: result.errors.password[0], classes: 'rounded red darken-3' });
            }
        } else {
            M.toast({ html: result.message, classes: 'rounded  teal accent-3' });
            setTimeout(function () {
                window.location.href = "login.html";
            }, 2000);
        }
    } catch (error) {
        console.log(error);
        M.toast({ html: 'Terjadi kesalahan pada server', classes: 'rounded red darken-3' });
    }
    // .then(response => response.json())
    // .then(response => {
    //     preloader.style.opacity = 1;
    //     preloader.style.display = 'none';
    //     if (response.message === 'The given data was invalid.') {
    //         if (response.errors.name) {
    //             M.toast({ html: response.errors.name[0], classes: 'rounded red darken-3' })
    //         }

    //         if (response.errors.email) {
    //             M.toast({ html: response.errors.email[0], classes: 'rounded red darken-3' })
    //         }

    //         if (response.errors.password) {
    //             M.toast({ html: response.errors.password[0], classes: 'rounded red darken-3' })
    //         }
    //     } else {
    //         M.toast({ html: response.message, classes: 'rounded  teal accent-3' });
    //         setTimeout(function () {
    //             window.location.href = "login.html"
    //         }, 2000)
    //     }
    // })
    // .catch(error => M.toast({ html: 'Terjadi kesalahan pada server', classes: 'rounded red darken-3' }))

}

async function verifyUserCaptcha(token) {
    try {
        let response = await fetch('http://localhost:8000/api/verifycaptcha', {
            'method': 'POST',
            'headers': {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
            },
            'body': JSON.stringify({
                'secret': '6LeyStIUAAAAAEUpzjC2rABwGGsE-LWExYRql9bh',
                'response': token
            })
        });

        let result = await response.json();
        console.log(result);
        let validUser = result.response.success;
        if (validUser) {
            register();
        } else {
            preloader.style.opacity = 1;
            preloader.style.display = 'none';
            M.toast({ html: 'Isi captcha terlebih dahulu', classes: 'rounded red darken-3' })
        }
    } catch (error) {
        console.log(error);
        M.toast({ html: 'Terjadi kesalahan pada server', classes: 'rounded red darken-3' });
    }
    // .then((result) => result.json())
    // .then((result) => {
    //     let validUser = result.response.success
    //     if (validUser) {
    //         register();
    //     } else {
    //         preloader.style.opacity = 1;
    //         preloader.style.display = 'none';
    //         M.toast({ html: 'Isi captcha terlebih dahulu', classes: 'rounded red darken-3' })
    //     }
    // })
    // .catch(error => M.toast({ html: 'Terjadi kesalahan pada server', classes: 'rounded red darken-3' }))
}


// function verifyUserCaptcha(token) {
//     fetch(`https://www.google.com/recaptcha/api/siteverify?secret=6LeyStIUAAAAAEUpzjC2rABwGGsE-LWExYRql9bh&response=${token}`)
//         .then((result) => result.json())
//         .then((result) => console.log(result))
//         .catch((err) => console.log(err));
// }
