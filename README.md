cara installasi backend
1. Clone project
2. Masuk ke direktori backend
3. ketikan 'composer install'
4. buat file '.env' di folder root project backend
5. setting nama database dan password sesuai dengan laptop atau komputer lokal anda
6. ketik 'php artisan key:generate'
7. ketik 'php artisan migrate'
8. ketik 'php artisan passport install'
9. jalan kan server 'php artisan serve'

cara installasi frontend
1. Pindahkan folder frontend ke directory localsever anda (ex: 'd:/xampp/htdocs')
2. Buka browser lalu ketikan 'localhost/frontend'
3. login jika punya akun. jika belum harap melakukan registrasi
4. selesai